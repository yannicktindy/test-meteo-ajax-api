// if (
// 	navigator.userAgent.match(/Android/i) ||
// 	navigator.userAgent.match(/iPhone/i)
//   )
//   if (/Android|iPhone/i.test(navigator.userAgent)) {
	
//   }  

let map = L.map('map').setView([43.812119, 4.357378], 14);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiY29yd3lubiIsImEiOiJjbDJvdWdzOGEyOHZnM21vN3hhYnkyZW12In0.XnqHdwatw1GGZEl_6YLskw'
}).addTo(map);

let markerSainte = L.marker([45.43, 4.39]).addTo(map);
let markerSaintJust = L.marker([45.50, 4.26]).addTo(map);
let markerFeurs = L.marker([45.77, 4.22]).addTo(map);
let markerNimes = L.marker([43.812119, 4.357378]).addTo(map);
let popUpNimes = L.popup()
.setLatLng([43.812119, 4.357378])
.setContent("La Nîmes'Alerie")
.openOn(map);



markerSaintJust.addEventListener('click', function() {
	getWeather('saint-just-saint-rambert');
})
markerSainte.addEventListener('click', function() {
	getWeather('saint-etienne-42');
})
markerFeurs.addEventListener('click', function() {
	getWeather('feurs');
})



function getWeather(endUrl) {
	const weatherRequest = new XMLHttpRequest();
	weatherRequest.open('GET', `https://www.prevision-meteo.ch/services/json/${endUrl}`);
	weatherRequest.send();

	weatherRequest.addEventListener('readystatechange', function(e)  {
		if (this.readyState === 4) {
			if (this.status === 200) {
				const response = JSON.parse(this.responseText);
				insertWeatherInDOM(response);
			}
		}
	});
}

function insertWeatherInDOM(datas) {
	// Le container qui va accueillir mon contenu
	let weatherContainer = document.querySelector('#weather');
	weatherContainer.classList.add('container', 'mt-3');
	weatherContainer.innerHTML = "";

	// Je créer les élements à injecter pour le jour courant
	const currentConditionContainer = document.createElement('div'); 
	const cityName = document.createElement('h2');
	const weatherImage = document.createElement('img');
	const weatherTemperatureParagraph = document.createElement('span');

	// Je leur metes les attributes et le contenu texte
	currentConditionContainer.classList.add('text-center', 'mx-auto')
	cityName.textContent = datas.city_info.name;
	weatherImage.src = datas.current_condition.icon_big;
	weatherTemperatureParagraph.textContent = `Température actuelle : ${datas.current_condition.tmp}°C`;
	currentConditionContainer.append(cityName, weatherImage, weatherTemperatureParagraph);
	
	// J'insère le div dans le dom
	weatherContainer.appendChild(currentConditionContainer);

	// Je m'occupe des jours suivants
	const nextConditionsContainer = document.createElement('div');
	nextConditionsContainer.classList.add('row', 'w-50', 'text-center', 'mx-auto')
	
	for (let property in datas) {
		if (/fcst_day_[1-4]{1}/.test(property)) {
			const cardDiv = document.createElement('div');
			const cardImg = document.createElement('img');
			const cardBody = document.createElement('div');
			const cardDay = document.createElement('h5');
			const cardTemperatureMin = document.createElement('span');
			const cardTemperatureMax = document.createElement('span');
			
			// Classes boostrap et autres attributs
			cardDiv.classList.add('card', 'col-3');
			cardImg.src = datas[property].icon_big;
			cardBody.classList.add('card-body');
			cardDay.textContent = datas[property].day_long;
			cardTemperatureMin.textContent = `Min : ${datas[property].tmin}°C `;
			cardTemperatureMax.textContent = `Max : ${datas[property].tmax}°C `;

			// J'ajoute les enfants
			cardBody.append(cardDay, cardTemperatureMax, cardTemperatureMin);
			cardDiv.append(cardImg, cardBody);
			nextConditionsContainer.appendChild(cardDiv);
		}
	}
	weatherContainer.appendChild(nextConditionsContainer);
}


let weatherButtons = document.querySelectorAll("#buttons-weather button");
weatherButtons.forEach(function(button){
	button.addEventListener('click', function() {
		getWeather(this.value);
	})
})
